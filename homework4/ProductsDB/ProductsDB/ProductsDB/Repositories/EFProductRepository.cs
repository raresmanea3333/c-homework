﻿using System.Collections.Generic;
using ProductsDB.DatabaseContext;
using ProductsDB.Models;

namespace ProductsDB.Repositories
{
    public class EFProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _context;
        public EFProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Product> Products => _context.Products;
    }
}
