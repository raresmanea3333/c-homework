﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductsDB.Repositories;

namespace ProductsDB.Models
{
    public class ProductRepository : IProductRepository
    {
        readonly IEnumerable<Product> _products = new List<Product>
        {
            new Product
            {
                Id = 1,
                Name = "Hi",
                Description = "jon"

            },
            new Product

            {
                Id = 2,
                Name = "water",
                Description = " is the best"

            },
            new Product
            {
                Id = 3,
                Name = "Jon",
                Description = " is healthy"

            }

        };

        public IEnumerable<Product> Products => _products;


        public Product GetById(int id)
        {
            return _products.SingleOrDefault(x => x.Id == id);
        }
    }
}

