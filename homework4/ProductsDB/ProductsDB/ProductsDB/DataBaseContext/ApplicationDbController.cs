﻿using Microsoft.EntityFrameworkCore;
using ProductsDB.Models;

namespace ProductsDB.DatabaseContext
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }


    }
}
