﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smecherie
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>();
            Person p1 = new Person
            {
                FirstName = "Copilu",
                LastName = "Minune",
                Id = 1
            };
            Student s = new Student
            {
                FirstName = "Cel",
                LastName = "Maibun", 
                Id = 1

            };
            personList.Add(p1);
            personList.Add(s);

            Console.WriteLine(personList);

            personList.OrderBy(x => x.FirstName).ThenBy(x => x.LastName).ToString();

            Console.WriteLine(personList);


        }
    }
}
