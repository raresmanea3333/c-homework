﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smecherie
{
    enum GradProfesor
    {
        grad_2 ,
        grad_1 ,
        doctorat
    };
    class Profesor : Person
    {
        public GradProfesor gradProfesor { get; set; }

    }
}
