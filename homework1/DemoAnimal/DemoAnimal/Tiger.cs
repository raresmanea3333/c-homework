﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAnimal
{
    class Tiger : Animal
    {
        public override string ToString() => $"Tiger info : {Id} {name} {description}";
    }
}
