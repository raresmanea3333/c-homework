﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAnimal
{
    public class Animal
    {
        public int Id { get; set; }
        public string name { get; set; }

        public string description { get; set; }

        public override string ToString() => $"Animal info : {Id} {name} {description}";
    }
}
