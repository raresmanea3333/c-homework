﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAnimal
{
    public class AnimalController
    {
        readonly IAnimalRepository _animalRepository;

        public AnimalController()
        {

        }

        public AnimalController(IAnimalRepository animalRepository) => _animalRepository = animalRepository;

        public void Insert(Animal a) => _animalRepository.add(a);

        public Animal edit(Animal a) => _animalRepository.edit(a);

        public IEnumerable<Animal> GetAll() => _animalRepository.GetAll();

        public void removeAnimal(int id) => _animalRepository.removeAnimal(id);
    }
}
