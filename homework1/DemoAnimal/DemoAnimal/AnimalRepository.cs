﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAnimal
{

    public interface IAnimalRepository
    {
        void add(Animal animal);
        Animal edit(Animal a);
        IEnumerable<Animal> GetAll();
        void removeAnimal(int id);

    }
    public class AnimalRepository : IAnimalRepository
    {
        readonly List<Animal> animals = new List<Animal>();
        public void add(Animal p) => animals.Add(p);
        public Animal edit(Animal a)
        {
            a.name = "john";
            a.Id = 3;
            a.description = "E paros";
            return a;

        }
        public IEnumerable<Animal> GetAll() => animals;

        public void removeAnimal(int id) => animals.RemoveAt(id);
    }
}
