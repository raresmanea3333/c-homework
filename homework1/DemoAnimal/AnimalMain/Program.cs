﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoAnimal;

namespace AnimalMain
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal p1 = new Animal
            {
                Id = 1,
                name = "dogy",
                description = "Dogy the dog"
            };

            Animal p2 = new Animal
            {
                Id = 2,
                name = "Jon",
                description = "Hatz"
            };

            AnimalRepository repo = new AnimalRepository();

            repo.add(p1);
            repo.add(p2);
            Animal first = repo.edit(p1);
            Animal second = repo.edit(p2);

            IEnumerable<Animal> animals = repo.GetAll();


            Console.WriteLine(first);
            Console.WriteLine(second);

            Console.WriteLine("Stop , pauzaaaaaa");

            foreach (Animal p in animals)
            {
                Console.WriteLine(p);
            }
        }
    }
}
