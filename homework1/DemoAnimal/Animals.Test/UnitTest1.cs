﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoAnimal;

namespace Persons.Test
{
    [TestClass]
    public class PersonTest
    {
        Animal p1 = new Animal
        {
            Id = 1,
            name = "dogy",
            description = "Dogy the dog"
        };

        Animal p2 = new Animal
        {
            Id = 2,
            name = "Jon",
            description = "Hatz"
        };
        [TestMethod]
        public void RetrieveCorectData_Test()
        {
            AnimalRepository repo = new AnimalRepository();

            repo.add(p1);
            repo.add(p2);
            Animal first = repo.edit(p1);
            Animal second = repo.edit(p2);

            Assert.AreEqual(p1.name, first.name);
            Assert.AreEqual(p1.description, first.description);
            Assert.AreEqual(p1.Id, first.Id);
        }

        //[TestMethod]
        //public void RetrieveInvalidData_Test()
        //{
        //    PresonRepository repo = new PresonRepository();

        //    repo.add(p1);
        //    repo.add(p2);
        //    Demo.Person first = repo.GetById(1);
        //    Demo.Person second = repo.GetById(2);

        //    Assert.AreEqual(p1.FirstName, first.FirstName);
        //    Assert.AreEqual(p1.LastName, first.LastName);
        //    Assert.AreEqual(3, first.Id);
        //}
    }
}

