﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoAnimal;

namespace Animals.Test
{
    class AnimalControllerTest
    {
        [TestMethod]

        public void GetData_Test()
        {
            Mock<IAnimalRepository> repoMock = new Mock<IAnimalRepository>();

            Animal p1 = new Animal
            {
                name = "john",
                description = "Smith",
                Id = 1
            };

            //repoMock.Setup(x => x.add(p1));
            repoMock.Setup(x => x.edit(p1)).Returns(p1);

            AnimalController controller = new AnimalController(repoMock.Object);

            Animal result = controller.edit(p1);

            Assert.AreEqual(p1.Id, result.Id);
            Assert.AreEqual(p1.name, result.name);
            Assert.AreEqual(p1.description, result.description);

        }

        [TestMethod]
        public void GetAllData_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();
            Animal p1 = new Animal
            {
                Id = 1,
                name = "dogy",
                description = "Dogy the dog"
            };

            Animal p2 = new Animal
            {
                Id = 2,
                name = "Jon",
                description = "Hatz"
            };

            List<Animal> people = new List<Animal>
            {
                p1,
                p2
            };

            mock.Setup(x => x.GetAll()).Returns(people);

            AnimalController controller = new AnimalController(mock.Object);

            List<Animal> result = controller.GetAll().ToList();

            Assert.AreEqual(2, result.Count);
        }
    }
}
