﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortStringByLetter
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> cityList = new List<string>()
            {
                "ROME",
                "LONDON",
                "NAIROBI",
                "CALIFORNIA",
                "ZURICH",
                "NEW DELHI",
                "AMSTERDAM",
                "ABU DHABI",
                "PARIS"
            };
            IEnumerable<string> query = cityList.Where(city =>( city.StartsWith("A")) && (city.EndsWith("M")));

            foreach(string city in query)
            {
                Console.WriteLine(city);
            }

            Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();

        }
    }
}
