﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tema3_ProductApp.Models
{
    public class ProductRepository
    {
        readonly List<Product> _products = new List<Product>
        {
            new Product
            {
                Id = 1,
                Name = "Salami",
                Description = "is the best."

            },
            new Product

            {
                Id = 2,
                Name = "Pudding",
                Description = " is for kids."

            },
            new Product
            {
                Id = 3,
                Name = "Water",
                Description = "is the source of life."

            }

        };

        public List<Product> Products => _products;

        public Product GetById(int id)
        {
            return _products.SingleOrDefault(x => x.Id == id);
        }
    }
}

